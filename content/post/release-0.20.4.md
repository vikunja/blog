---
title: "Vikunja frontend v0.20.4 and API v0.20.3 was released"
date: 2023-03-10T14:58:56+01:00
draft: false
---

<div class="bg-amber-400 p-4 rounded mt-4">
There has been a <a href="{{< ref "./release-0.20.5.md">}}" class="!underline">patch release for the frontend and api</a> with additional fixes.
Updating is highly encouraged.
</div>

This is a patch release with important fixes and improvements in the frontend and api.

Among code-improvements, refactorings and dependency updates, these are the noteworthy changes:

* The docker images for both parts have been improved under the hood - they should work the same way.
* Empty deletion dates are now properly set when restoring from a Vikunja dump. This caused some users to be deleted in the past.
* Some migration problems with Todoist and TickTick have been resolved.
* After using the kanban board for a while, some tasks would get the position 0 and thus wouldn't be sorted correctly anymore. This is now fixed.
* CalDAV now supports managing labels via its categories property.
* Logging for events is now off by default as it has very little value most of the time.
* When configuring a language other than english, the sidebar would not collapse properly on mobile. This is now fixed.
* The sort order for tasks in table view is now correctly saved and loaded.

In other news, there is now an [official Vikunja Helm Chart](https://kolaente.dev/vikunja/helm-chart/)!
If you're running (or want to) Vikunja in a k8s environment, we highly reccomend using this chart.

Thanks to [Yurii Vlasov](https://kolaente.dev/vlasov-y) for leading the effort here.

As usual, you can find the full changelogs in the [frontend](https://kolaente.dev/vikunja/frontend/src/branch/main/CHANGELOG.md#0-20-4-2023-03-10) and [api](https://kolaente.dev/vikunja/api/src/branch/main/CHANGELOG.md#0-20-3-2023-03-10) repos.

To get the upgrade, simply replace the frontend files or api binary or pull the latest docker image.
You can also check out the update docs for the [frontend](https://vikunja.io/docs/install-frontend/#updating) or [api](https://vikunja.io/docs/install-backend/#updating) updating process.

