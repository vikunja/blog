---
title: "[API] Vikunja 0.19.2 is released"
date: 2022-08-17T17:06:09+02:00
draft: false
---

This release fixes a critical bug which was introduced in the api in 0.19.1.
The bug would replace all saved filters of all users with the last one.

**Updating is highly recomended.**

To upgrade, replace the api binary with the latest one from [the downloads page](https://dl.vikunja.io/api/0.19.2) or pull the [latest docker image](https://hub.docker.com/r/vikunja/api).

